import os,re
import json,urllib2
from datetime import datetime
import tweepy
import datetime as DT
import time
import ConfigParser

def get_list_of_app(app_list):
    playrest_url = 'http://localhost:3000/api/'

    with open(app_list, 'r') as f:
        tweets = []
        for line in f.readlines():
            app_playstore_data = {}
            print line
            try:
                app_playstore_data = json.loads(urllib2.urlopen(playrest_url + 'apps/' + line.lstrip().rstrip()).read())
            except urllib2.HTTPError:
                print urllib2.HTTPError.reason
                continue

            if app_playstore_data.has_key('updated') and datetime.strptime(app_playstore_data['updated'],'%B %d, %Y').date() > (datetime.today() - DT.timedelta(days = 4)).date():
                tweet_text = '#UPI #AppUpdate ' + app_playstore_data['title'] + ' ' \
                      + app_playstore_data['playstoreUrl'][:app_playstore_data['playstoreUrl'].find('&')] \
                      + ' ' + ''.join(re.escape(app_playstore_data['recentChanges']))[:(95 - len(app_playstore_data['title']))] \
                      + '...'
                tweets.append(tweet_text)
            elif app_playstore_data.has_key('title'):
                print ' '.join((app_playstore_data['title'],app_playstore_data['updated'])).encode('utf-8').strip()
                print '\n'
            else:
                tweet_text = '#UPI App not found ' + line
                tweets.append(tweet_text)
        return tweets

def get_api(cfg):
  auth = tweepy.OAuthHandler(cfg['consumer_key'], cfg['consumer_secret'])
  auth.set_access_token(cfg['access_token'], cfg['access_token_secret'])
  return tweepy.API(auth)

if __name__ == '__main__':

    cfg = {
        "consumer_key": os.getenv('CC_TWITTER_CONSUMER_KEY'),
        "consumer_secret": os.getenv('CC_TWITTER_CONSUMER_SECRET'),
        "access_token": os.getenv('CC_TWITTER_ACCESS_TOKEN'),
        "access_token_secret": os.getenv('CC_TWITTER_ACCESS_TOKEN_SECRET')
    }
    api = get_api(cfg)

    tweets = get_list_of_app('UPIAppList.txt')
    print tweets

    if 'no' == 'yes':
        prev_status = None
        for tweet in tweets:
            if prev_status is not None:
                prev_status = api.update_status(status=tweet, in_reply_to_status_id=prev_status.id)
            else:
                prev_status = api.update_status(status=tweet)