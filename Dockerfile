FROM srikanthlogic/bionic-playproxy:latest

ADD requirements.txt $WORKDIR/

RUN pip install --upgrade pip

RUN pip install --trusted-host pypi.python.org -r $WORKDIR/requirements.txt

EXPOSE 3000
