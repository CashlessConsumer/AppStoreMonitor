from datetime import datetime
import datetime as DT
import json,jsondiff,urllib2
import collections
import tweepy
import os,errno
import ConfigParser

__author__ = "Srikanth"


def merge_dict(d1, d2):
    """
    Modifies d1 in-place to contain values from d2.  If any value
    in d1 is a dictionary (or dict-like), *and* the corresponding
    value in d2 is also a dictionary, then merge them in-place.
    """
    for k,v2 in d2.items():
        v1 = d1.get(k) # returns None if v1 has no value for this key
        if ( isinstance(v1, collections.Mapping) and
             isinstance(v2, collections.Mapping) ):
            merge_dict(v1, v2)
        else:
            d1[k] = v2


def get_recent_changes(app_bundle):

    with open(app_bundle['list'], 'r') as f:
        tweets = []
        for line in f.readlines():
            app_playstore_data = {}
            line = line.lstrip().rstrip()
            line_metadata_path = os.path.join('./', app_bundle['folder'], line + '.json')

            print line
            try:
                app_playstore_data = json.loads(urllib2.urlopen(playrest_url + 'apps/' + line.lstrip().rstrip()).read())
            except urllib2.HTTPError:
                print urllib2.HTTPError.reason
                continue

            if app_playstore_data.has_key('updated') and datetime.strptime(app_playstore_data['updated'],'%B %d, %Y').date() > (datetime.today() - DT.timedelta(days = 2)).date():
                tweet_text = app_bundle['hashtag'] + '#AppUpdate ' + app_playstore_data['title'] + ' ' \
                      + app_playstore_data['playstoreUrl'][:app_playstore_data['playstoreUrl'].find('&')] \
                      + ' ' + ''.join(app_playstore_data['recentChanges'])[:(195 - len(app_playstore_data['title']))] \
                      + '...'
                tweets.append(tweet_text)
            elif app_playstore_data.has_key('title'):
                print app_playstore_data['title'] + ' ' + app_playstore_data['updated'] + '\n'
            else:
                tweet_text = '#UPI App not found ' + line
                tweets.append(tweet_text)
        return tweets


def get_twitter_api(cfg):
    auth = tweepy.OAuthHandler(cfg['consumer_key'], cfg['consumer_secret'])
    auth.set_access_token(cfg['access_token'], cfg['access_token_secret'])
    return tweepy.API(auth)


def update_metadata(app_bundle, play_store_api):

    tweets = []
    with open(app_bundle['list'],'r') as f:
        for line in f.readlines():
            line = line.lstrip().rstrip()
            line_metadata_path = os.path.join('./', app_bundle['folder'], line + '.json')

            try:
                app_playstore_data = json.loads(urllib2.urlopen(play_store_api + '/apps/' + line).read())
            except urllib2.HTTPError:
                print urllib2.HTTPError.reason
                continue

            if os._exists(line_metadata_path):
                try:
                    with open(line_metadata_path) as jsonappdata:
                        local_app_playstore_data = json.load(jsonappdata)
                        jsonappdata.close()
                except:
                    print 'Error loading local app data'
                    continue

                delta_appdata = jsondiff.diff(local_app_playstore_data,app_playstore_data)
                if 'comments' in delta_appdata:
                    del delta_appdata['comments']
                delta_appdata['date'] = DT.date.today().isoformat()
                if local_app_playstore_data.has_key('extended'):
                    app_playstore_data['extended'] = local_app_playstore_data['extended']

                if not app_playstore_data.has_key('extended'):
                    app_playstore_data['extended'] = {}
                if not app_playstore_data['extended'].has_key('delta'):
                    app_playstore_data['extended']['delta'] = []
                app_playstore_data['extended']['delta'].append(delta_appdata)
                print 'Delta'
                print app_playstore_data['extended']['delta']

            else:

                if not os.path.exists(os.path.dirname(line_metadata_path)):
                    try:
                        os.makedirs(os.path.dirname(line_metadata_path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise

            if (app_bundle['tweet']):
                if app_playstore_data.has_key('updated') and datetime.strptime(app_playstore_data['updated'],'%B %d, %Y').date() > (datetime.today() - DT.timedelta(days = 2)).date():
                    tweet_text = app_bundle['hashtag'] + '#AppUpdate ' + app_playstore_data['title'] + ' ' \
                          + app_playstore_data['playstoreUrl'][:app_playstore_data['playstoreUrl'].find('&')] \
                          + ' ' + ''.join(re.escape(app_playstore_data['recentChanges']))[:(235 - len(app_playstore_data['title']))] \
                          + '...'
                    tweets.append(tweet_text)

            with open(line_metadata_path, 'w') as fh:
                json.dump(app_playstore_data, fh)
    return tweets


def main():

    config = ConfigParser.ConfigParser()
    config.read('config.ini')

    cfg = {
        "consumer_key": os.getenv('CC_TWITTER_CONSUMER_KEY'),
        "consumer_secret": os.getenv('CC_TWITTER_CONSUMER_SECRET'),
        "access_token": os.getenv('CC_TWITTER_ACCESS_TOKEN'),
        "access_token_secret": os.getenv('CC_TWITTER_ACCESS_TOKEN_SECRET')
    }

    api = get_twitter_api(cfg)

    app_bundles = []
    with open(config.get('DEFAULT', 'APP_LISTS')) as f:
        app_bundles = json.load(f)
    playstore_api = config.get('DEFAULT','PLAY_REST_API')

    bundle_tweets_list = []
    for bundle in app_bundles:
        bundle_tweets_list.append(update_metadata(bundle,playstore_api))

    bundle_tweets_list = filter(None, bundle_tweets_list)

    prev_status = None
    for bundle_tweets in bundle_tweets_list:
        for tweet in bundle_tweets:
            if prev_status is not None:
                prev_status = api.update_status(status=tweet, in_reply_to_status_id=prev_status.id)
            else:
                prev_status = api.update_status(status=tweet)

if __name__ == '__main__':
    main()
