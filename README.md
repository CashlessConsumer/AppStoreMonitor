#Play Store Metadata Achiever


##About

Play Store Metadata Achiever uses the [google-play-api](https://github.com/facundoolano/google-play-api) to query apps on [Google Play Store](http://play.google.com/apps) and store the app metadata periodically. It also has feature to tweet recent updates to any of the apps present in app list.

##TODO
* App Permission metadata
* Archive old app metadata 
* Recent Changes Log, Email
* Developer watch for addition / removal of apps.

##Credits
* [google-play-api](https://github.com/facundoolano/google-play-api)

