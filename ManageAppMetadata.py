import datetime as dt
from datetime import datetime
import json,jsondiff,urllib2,tweepy
import collections
import os,errno

def merge_dict(d1, d2):
    """
    Modifies d1 in-place to contain values from d2.  If any value
    in d1 is a dictionary (or dict-like), *and* the corresponding
    value in d2 is also a dictionary, then merge them in-place.
    """
    for k,v2 in d2.items():
        v1 = d1.get(k) # returns None if v1 has no value for this key
        if ( isinstance(v1, collections.Mapping) and
             isinstance(v2, collections.Mapping) ):
            merge_dict(v1, v2)
        else:
            d1[k] = v2


def update_metadata(app_bundle, play_store_api):

    tweets = []

    with open(app_bundle['list'],'r') as f:
        for line in f.readlines():
            line = line.lstrip().rstrip()
            line_metadata_path = os.path.join('./AppData', line + '.json')

            try:
                app_playstore_data = json.loads(urllib2.urlopen(play_store_api + '/apps/' + line).read())
            except urllib2.HTTPError:
                print urllib2.HTTPError.reason
                continue
            if 'comments' in app_playstore_data:
                    del app_playstore_data['comments']
            tweet_days = int(os.getenv('CC_APPSTORE_MONITOR_TWEETDAYS'))

            if app_bundle['tweet'] == 'True' and app_playstore_data.has_key('updated') and datetime.fromtimestamp(int(app_playstore_data['updated'])/1000).date() > (datetime.today() - dt.timedelta(days = tweet_days)).date():
                tweet_text = '#' + app_bundle['hashtag'] + ' #AppUpdate ' + app_playstore_data['title'] + ' ' \
                      + app_playstore_data['playstoreUrl'][:app_playstore_data['playstoreUrl'].find('&')] \
                      + ' ' + ''.join(app_playstore_data['recentChanges'])[:(195 - len(app_playstore_data['title']))] \
                      + '...'
                tweets.append(tweet_text)

            if os.path.isfile(line_metadata_path):
                try:
                    with open(line_metadata_path) as jsonappdata:
                        local_app_playstore_data = json.load(jsonappdata)
                        jsonappdata.close()
                except:
                    print 'Error loading local app data ' + line_metadata_path
                    continue

                delta_appdata = jsondiff.diff(local_app_playstore_data,app_playstore_data)
                if 'comments' in delta_appdata:
                    del delta_appdata['comments']
                delta_appdata['date'] = dt.date.today().isoformat()
                if local_app_playstore_data.has_key('extended'):
                    app_playstore_data['extended'] = local_app_playstore_data['extended']

                if not app_playstore_data.has_key('extended'):
                    app_playstore_data['extended'] = {}

                # Permission data into extended tag.
                try:
                    app_playstore_data_perms = json.loads(urllib2.urlopen(play_store_api + '/apps/' + line + '/permissions').read())
                except urllib2.HTTPError:
                    print urllib2.HTTPError.reason
                    continue
                if not app_playstore_data['extended'].has_key('permissions'):
                    app_playstore_data['extended']['permissions'] = app_playstore_data_perms
                else:
                    app_playstore_data_perms_local = local_app_playstore_data['extended']['permissions']
                    delta_appdata_perms = jsondiff.diff(app_playstore_data_perms_local,app_playstore_data_perms)
                    if len(delta_appdata_perms.keys()) != 0:
                        delta_appdata['permissions'] = delta_appdata_perms

                if not app_playstore_data['extended'].has_key('delta'):
                    app_playstore_data['extended']['delta'] = []
                app_playstore_data['extended']['delta'].append(str(delta_appdata))

            else:
                if not os.path.exists(os.path.dirname(line_metadata_path)):
                    try:
                        os.makedirs(os.path.dirname(line_metadata_path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise

            with open(line_metadata_path, 'w') as fh:
                json.dump(app_playstore_data, fh)
    return tweets


# Get other untracked apps by developers of existing apps
def get_new_apps(appbundles):
    all_apps = []
    known_apps = []

    with open('KnownApps.txt','r') as f:
        for line in f.readlines():
            line = line.lstrip().rstrip()
            known_apps.append(line)

    for bundle in appbundles:
        with open(bundle['list'], 'r') as f:
            for line in f.readlines():
                line = line.lstrip().rstrip()
                all_apps.append(line)

    all_developers = []
    for app in all_apps:
        line_metadata_path = os.path.join('./AppData', app + '.json')
        try:
            with open(line_metadata_path) as jsonappdata:
                local_app_playstore_data = json.load(jsonappdata)
                jsonappdata.close()
        except:
            print 'Error loading local app data ' + line_metadata_path
            continue
        all_developers.append(local_app_playstore_data['developer']['url'])

    all_developers = list(set(all_developers))

    all_apps_from_playstore = []
    for devel in all_developers:
        try:
            app_playstore_dev_apps = json.loads(urllib2.urlopen(devel).read())
        except urllib2.HTTPError:
            print urllib2.HTTPError.reason
            continue
        for app in app_playstore_dev_apps['apps']:
            all_apps_from_playstore.append(app['appId'])
    new_apps = list(set(all_apps_from_playstore) - set(all_apps) - set(known_apps))
    print 'New apps not monitored -- ' + str(len(new_apps))
    for new_app in new_apps:
        print new_app


def get_twitter_api(cfg):
    auth = tweepy.OAuthHandler(cfg['consumer_key'], cfg['consumer_secret'])
    auth.set_access_token(cfg['access_token'], cfg['access_token_secret'])
    return tweepy.API(auth)


def main():

    app_bundles = []
    #with open(config.get('DEFAULT', 'APP_LISTS')) as f:
    with open('AppCollections.json') as f:
        app_bundles = json.load(f)
    #playstore_api = config.get('DEFAULT','PLAY_REST_API')
    playstore_api = 'https://gplayapi.herokuapp.com/api'

    cfg = {
        "consumer_key": os.getenv('CC_TWITTER_CONSUMER_KEY'),
        "consumer_secret": os.getenv('CC_TWITTER_CONSUMER_SECRET'),
        "access_token": os.getenv('CC_TWITTER_ACCESS_TOKEN'),
        "access_token_secret": os.getenv('CC_TWITTER_ACCESS_TOKEN_SECRET')
    }

    api = get_twitter_api(cfg)

    bundle_tweets_list = []

    if( os.getenv('CC_APPSTORE_MONITOR_UPDATE_METADATA') == 'True'):
        for bundle in app_bundles:
            bundle_tweets_list.append(update_metadata(bundle,playstore_api))

    bundle_tweets_list = filter(None, bundle_tweets_list)

    get_new_apps(app_bundles)

    prev_status = None
    for bundle_tweets in bundle_tweets_list:
        for tweet in bundle_tweets:
            if prev_status is not None:
                prev_status = api.update_status(status=tweet, in_reply_to_status_id=prev_status.id)
            else:
                prev_status = api.update_status(status=tweet)


if __name__ == '__main__':
    main()
